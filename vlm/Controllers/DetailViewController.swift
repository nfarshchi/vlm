//
//  DetailViewController.swift
//  vlm
//
//  Created by Navid Farshchi on 20/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController: UIViewController {
    
    var pattern : PatternViewModel?
    
    var titleLabel = UILabel()
    var dateLabel = UILabel()
    var patternImageView = UIImageView()
    var numberOfLoveLabel = UILabel()
    var numberOfLoveTitleLabel = UILabel()
    var numberOfCommentLabel = UILabel()
    var numberOfCommentTitleLabel = UILabel()
    var numberOfVotesLabel = UILabel()
    var numberOfVotesTitleLabel = UILabel()
    var userNameLabel = UILabel()
    var colorViewsArray : [UIView] = []
    var numberStackView = UIStackView()
    var colorsStackView = UIStackView()
    var descriptionTextView = UITextView()
    
    
    init(pattern: PatternViewModel?) {
        self.pattern = pattern
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createUIElements()
        if let pattern = self.pattern {
            self.fillWithData(pattern)
        }
    }
    
    func setData(_ data : PatternViewModel) {
        self.pattern = data
    }
    
    func createUIElements() {
        //Creat close button
        let closeButton = UIButton(type: .system)
        closeButton.setTitle("Back", for: .normal)
        closeButton.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        self.view.backgroundColor = Constants.Colors.cellBackgroundColor
        self.view.addSubview(closeButton)
        
        closeButton.anchor(top: self.view.topAnchor, left: nil, bottom: nil, right: self.view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 70, heightConstant: 40)
        
        numberStackView = UIStackView(arrangedSubviews: [numberOfLoveLabel, numberOfLoveTitleLabel, numberOfVotesLabel, numberOfVotesTitleLabel, numberOfCommentLabel, numberOfCommentTitleLabel])
        
        titleLabel.applyTitleStyle()
        dateLabel.applyDateStyle()
        numberOfLoveLabel.applyNumberStyle()
        numberOfVotesLabel.applyNumberStyle()
        numberOfCommentLabel.applyNumberStyle()
        numberOfLoveTitleLabel.applyNumberTitleStyle()
        numberOfVotesTitleLabel.applyNumberTitleStyle()
        numberOfCommentTitleLabel.applyNumberTitleStyle()
        userNameLabel.applyUserNameStyle()
        numberStackView.applyVerticalStackViewStyle()
        descriptionTextView.applyDescriptionTextViewStyle()
        colorsStackView.applyHorizontalStackViewStyle()
        
        self.view.addSubview(titleLabel)
        self.view.addSubview(dateLabel)
        self.view.addSubview(patternImageView)
        self.view.addSubview(numberStackView)
        self.view.addSubview(colorsStackView)
        self.view.addSubview(userNameLabel)
        self.view.addSubview(descriptionTextView)
        
        
        numberStackView.applyHorizontalStackViewStyle()
        
        titleLabel.anchor(top: closeButton.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 30)
        titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        userNameLabel.anchor(top: titleLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        userNameLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        dateLabel.anchor(top: userNameLabel.bottomAnchor, left: nil, bottom: nil, right: nil, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0 , heightConstant: 15)
        dateLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        
        patternImageView.anchor(top: dateLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        
        numberStackView.anchor(top: patternImageView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 5, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 30)
        
        colorsStackView.anchor(top: numberStackView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 25)
        
        descriptionTextView.anchor(top: colorsStackView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 150)
        
    }
    
    func fillWithData(_ data : PatternViewModel) {
        
        titleLabel.text = data.pattern.title
        dateLabel.text = data.pattern.dateCreated
        
        numberOfCommentTitleLabel.text = Constants.strings.COMMENTS_TITLE
        numberOfVotesTitleLabel.text = Constants.strings.VOTES_TITLE
        numberOfLoveTitleLabel.text = Constants.strings.LOVES_TITLE
        
        numberOfLoveLabel.text = "\(data.pattern.numHearts)"
        numberOfVotesLabel.text = "\(data.pattern.numVotes)"
        numberOfCommentLabel.text = "\(data.pattern.numComments)"
        
        userNameLabel.text = "By: \(data.pattern.userName)"
        
        //Load image Async and save data for later smooth loading
        
        patternImageView.applyPatternImageViewStyle()
        
        self.patternImageView.image = UIImage.init(data: data.imageData!)
        
        
        
        //Clear colorStackView
        DispatchQueue.main.async {
            for view in self.colorsStackView.subviews {
                view.removeFromSuperview()
            }
            //Add some view with background colors to colorStackView
            for i in 0..<data.pattern.colors.count {
                let tempView = UILabel()
                tempView.text = " "
                tempView.backgroundColor = hexStringToUIColor(hex: data.pattern.colors[i])
                tempView.layer.cornerRadius = 5
                tempView.clipsToBounds = true
                self.colorsStackView.addArrangedSubview(tempView)
            }
        }
        
        self.descriptionTextView.attributedText = data.attributedDescription
        
    }
    
    @objc func closeAction() {
        dismiss(animated: true, completion: nil)
    }
    
}
