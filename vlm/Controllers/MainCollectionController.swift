//
//  ViewController.swift
//  vlm
//
//  Created by Navid Farshchi on 18/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class MainCollectionController: UICollectionViewController, UICollectionViewDelegateFlowLayout, DataManagerDelegate {
    
    private var activityIndicator : UIActivityIndicatorView!
    
//    var detailViewController : DetailViewController?
    let cellID = "PatternCollectionViewCellID"
//    let cellStyle2ID = "PatternCollectionViewCellStyle2ID"
//    let cellStyle3ID = "PatternCollectionViewCellStyle3ID"

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        collectionView?.register(PatternCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
//        collectionView?.register(PatternCollectionViewCellStyleCenter.self, forCellWithReuseIdentifier: cellStyle2ID)
//        collectionView?.register(PatternCollectionViewCellStyleRight.self, forCellWithReuseIdentifier: cellStyle3ID)
        collectionView?.alwaysBounceVertical = true
        self.navigationItem.title = "vlm"
        addActivityIndicator()
        
        DataManager.shared.registerDataDelegate(self)
        DataManager.shared.initialize()
        
    }
    
    func addActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.activityIndicatorViewStyle = .gray
        let barButton = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }
    
    //MARK: - UICollectionView Delegates
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.shared.dataArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(   withReuseIdentifier: cellID, for: indexPath) as! PatternCollectionViewCell
        
        cell.fillWithData(DataManager.shared.dataArray[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height : CGFloat = 0
        switch (DataManager.shared.dataArray[indexPath.row].styleCell) {
        case .centerStyle:
            height = Constants.sizes.ROW_HEIGHT_STYLE_CENTER
        case .leftStyle:
            height = Constants.sizes.ROW_HEIGHT_STYLE_LEFT
        case .rightStyle:
            height = Constants.sizes.ROW_HEIGHT_STYLE_RIGHT
        }
        
        return CGSize(width: view.frame.width, height: height)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (DataManager.shared.dataArray[indexPath.item].pattern.numComments > 0) {
            //Show detail view controller
            print("tapped")
            
            let detailViewController = DetailViewController(pattern: DataManager.shared.dataArray[indexPath.item])
            detailViewController.modalTransitionStyle = .flipHorizontal
            present(detailViewController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - DataManager Delegates
    func onDataReady() {
        DispatchQueue.main.async {
            let contentOffset = self.collectionView?.contentOffset
            self.collectionView?.reloadData()
            self.collectionView?.setContentOffset(contentOffset!, animated: true)
        }
    }
    
    func onDataReadingInProgress() {
        activityIndicator.startAnimating()
    }
    
    func onDataReadingFinished() {
        activityIndicator.stopAnimating()
    }
    
    func showErrorMessage(message : String) {
        
    }

}

