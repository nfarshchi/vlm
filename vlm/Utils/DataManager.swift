//
//  DataManager.swift
//  vlm
//
//  Created by Navid Farshchi on 18/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//
//TODO: no internet handle
//TODO: connection interuption handle


import Foundation

class DataManager : NetworkDelegate, XMLToObjectDelegate, WSDelegate {
    
    private init() { }
    
    static let shared = DataManager()
    var dataArray : [PatternViewModel] = []
    
    var dataDelegate : DataManagerDelegate!
    
    func registerDataDelegate(_ delegate : DataManagerDelegate) {
        self.dataDelegate = delegate
    }
    
    //MARK: - initialize
    func initialize() {
        //Get Data every 15 second
        let randomInterval = randomBetween(lowerBound: 15, upperBound: 30)
        readData()
        Timer.scheduledTimer(timeInterval: Double(randomInterval), target: self, selector:#selector(readData), userInfo: nil, repeats: true)
        
        WebSocketManager.shared.initialize()
        WebSocketManager.shared.registerDelegate(self)

        XMLManager.shared.registerDelegate(self)
    }
    
    @objc func readData() {
        getRequest(server: Constants.SERVER, request: Constants.REQUEST, delegate: self)
    }
    
    //MARK: - WebSocket Manager Delegate method
    func onDataFromWebSocket(_ data: Data) {
        XMLManager.shared.parseXML(xmlData: data)
    }
    
    
    //MARK: - XMLToObjectDelegate method
    func onConvertFromXML(object: PatternViewModel) {
        dataArray.append(object)
        dataDelegate.onDataReadingFinished()
        dataDelegate.onDataReady()
    }
    
    //MARK: - Network delegates methods
    func onSuccessWithResponse(resonse: Data) {
        WebSocketManager.shared.writeDataOnSocket(resonse)
        print("Success service call")
    }
    
    func onFailedWithError(error: Error) {
        print("failed service call with error \(error.localizedDescription)")
    }
    
    func serviceCallOnProgress() {
        print("startServiceCall")
        dataDelegate.onDataReadingInProgress()
    }
    
    func serviceCallFinished() {
        print("endServiceCall")
        
    }
    
}

protocol DataManagerDelegate {
    func onDataReady()
    func onDataReadingInProgress()
    func onDataReadingFinished()
}
