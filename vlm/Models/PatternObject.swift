//
//  PatternObject.swift
//  vlm
//
//  Created by Navid Farshchi on 19/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

class Pattern {
    var id:Int = 0
    var title : String = ""
    var userName : String = ""
    var numViews : Int = 0
    var numVotes : Int = 0
    var numComments : Int = 0
    var numHearts : Int = 0
    var rank : Int = 0
    var dateCreated : String = ""
    var colors : [String] = []
    var description : String = ""
    var url : String = ""
    var imageUrl : String = ""
    var badgeUrl : String = ""
    var apiUrl : String = ""
    var templete = Template()
}

class Template {
    var title : String = ""
    var url : String = ""
    var author = Author()
}

class Author {
    var userName : String = ""
    var url : String = ""
}

enum PatternCellStyle : Int {
    case leftStyle = 0
    case centerStyle = 1
    case rightStyle = 2
}
