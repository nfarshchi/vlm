//
//  PatternViewModel.swift
//  vlm
//
//  Created by Navid Farshchi on 23/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

class PatternViewModel : NSObject {
    let pattern : Pattern!

    var imageData : Data? = nil
    var attributedDescription : NSAttributedString? = nil
    var styleCell: PatternCellStyle
    
    init(withPattern pattern: Pattern, style : PatternCellStyle) {
        self.pattern = pattern
        self.styleCell = style
        super.init()
        
        self.downloadImageData()
        self.createAttributedString()
    }
    
    private func downloadImageData() {
        URLSession.shared.dataTask( with: NSURL(string:self.pattern.imageUrl)! as URL, completionHandler: {
            (imageData, response, error) -> Void in
            DispatchQueue.main.async {
                if let imgdata = imageData {
                    self.imageData = imgdata
                }
            }
        }).resume()
    }
    
    func createAttributedString() {
        DispatchQueue.global(qos: .background).async {
            let htmlData = NSString(string: self.pattern.description).data(using: String.Encoding.unicode.rawValue)
            let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
            let attributedString = try! NSAttributedString(data: htmlData!, options: options, documentAttributes: nil)
            self.attributedDescription = attributedString
        }
    }
    
}
